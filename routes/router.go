package routes

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/BIC_Dev/nitrado-service/controllers"
)

// GetRouter creates and returns a router
func GetRouter() *mux.Router {
	return mux.NewRouter().StrictSlash(true)
}

// AddRoutes adds all necessary routes to the router
func AddRoutes(router *mux.Router, c *controllers.Controller) {
	router.HandleFunc("/nitrado-service/status", c.GetStatus).Methods("GET")

	router.HandleFunc("/nitrado-service/gameserver/{gameserver_id}", c.GetGameserver).Methods("GET")

	router.HandleFunc("/nitrado-service/service", c.GetAllServices).Methods("GET")
	router.HandleFunc("/nitrado-service/service/{gameserver_id}", c.GetOneService).Methods("GET")
}

// StartListener starts the HTTP listener
func StartListener(router *mux.Router) {
	log.Fatal(http.ListenAndServe(":8080", router))
}

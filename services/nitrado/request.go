package nitrado

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/BIC_Dev/nitrado-service/utils"
)

// Timeout default timeout value
const Timeout = 10

// Request handler
func Request(method string, url string, params map[string]string, body interface{}, headers map[string]string) (*Response, *utils.RequestError) {
	httpClient := &http.Client{
		Timeout: time.Second * Timeout,
	}

	var requestBody *bytes.Buffer
	var newRequest *http.Request
	var newRequestErr error

	if body != nil {
		jsonBody, jsonErr := json.Marshal(body)

		if jsonErr != nil {
			requestErr := utils.NewRequestError(jsonErr)
			requestErr.SetMessage("Could not marshal request struct")
			requestErr.SetStatus(http.StatusInternalServerError)
			return nil, requestErr
		}

		requestBody = bytes.NewBuffer(jsonBody)

		newRequest, newRequestErr = http.NewRequest(method, url, requestBody)
	} else {
		newRequest, newRequestErr = http.NewRequest(method, url, nil)
	}

	if newRequestErr != nil {
		requestErr := utils.NewRequestError(newRequestErr)
		requestErr.SetMessage("Failed to create request")
		requestErr.SetStatus(http.StatusInternalServerError)
		return nil, requestErr
	}

	for k, v := range headers {
		newRequest.Header.Add(k, v)
	}

	response, requestErr := httpClient.Do(newRequest)

	if requestErr != nil {
		requestErr := utils.NewRequestError(requestErr)
		requestErr.SetMessage("Request failed")
		requestErr.SetStatus(http.StatusBadRequest)
		return nil, requestErr
	}

	switch response.StatusCode {
	case http.StatusOK:
		var responseStruct *Response
		jsonErr := json.NewDecoder(response.Body).Decode(&responseStruct)

		if jsonErr != nil {
			requestErr := utils.NewRequestError(jsonErr)
			requestErr.SetMessage("Invalid JSON data from Nitrado API")
			requestErr.SetStatus(http.StatusInternalServerError)
			return nil, requestErr
		}

		return responseStruct, nil
	default:
		requestErr := utils.NewRequestError(fmt.Errorf("Failure response: (%d)", response.StatusCode))
		requestErr.SetMessage("Bad response from Nitrado API")
		requestErr.SetStatus(response.StatusCode)
		return nil, requestErr
	}
}

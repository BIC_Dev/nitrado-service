package nitrado

// Response struct
type Response struct {
	Status string `json:"status"`
	Data   Data   `json:"data"`
}

// Data struct
type Data struct {
	Services      []Service  `json:"services;omitempty"`
	Service       Service    `json:"service;omitempty"`
	Boosts        []Boost    `json:"boosts;omitempty"`
	Gameserver    Gameserver `json:"gameserver;omitempty"`
	CurrentPage   int        `json:"current_page;omitempty"`
	BoostsPerPage int        `json:"boosts_per_page;omitempty"`
	PageCount     int        `json:"page_count;omitempty"`
	BoostCount    int        `json:"boosts_count;omitempty"`
}

// Service struct
type Service struct {
	ID                    int64  `json:"id"`
	LocationID            int    `json:"location_id"`
	Status                string `json:"status"`
	WebsocketToken        string `json:"websocket_token"`
	UserID                int64  `json:"user_id"`
	Comment               string `json:"comment"`
	AutoExtension         bool   `json:"auto_extension"`
	AutoExtensionDuration int    `json:"auto_extension_duration"`
	Type                  string `json:"type"`
	TypeHuman             string `json:"type_human"`
	Details               struct {
		Address       string `json:"address"`
		Name          string `json:"name"`
		Game          string `json:"game"`
		PortlistShort string `json:"portlist_short"`
		FolderShort   string `json:"folder_short"`
		Slots         int    `json:"slots"`
	} `json:"details"`
	StartDate    string   `json:"start_date"`
	SuspendDate  string   `json:"suspend_date"`
	DeleteDate   string   `json:"delete_date"`
	SuspendingIn int      `json:"suspending_in"`
	DeletingIn   int      `json:"deleting_in"`
	Username     string   `json:"username"`
	Roles        []string `json:"roles"`
}

// Boost struct
type Boost struct {
	ID          int    `json:"id"`
	Username    string `json:"username"`
	ExtendedFor int    `json:"extended_for"`
	Amount      string `json:"amount"`
	Message     string `json:"message"`
	BoostedAt   string `json:"boosted_at"`
}

// Gameserver struct
type Gameserver struct {
	MustBeStarted    bool   `json:"must_be_started"`
	Status           string `json:"status"`
	LastStatusChange int    `json:"last_status_change"`
	WebsocketToken   string `json:"websocket_token"`
	Hostsystems      struct {
		Linux struct {
			Hostname string `json:"hostname"`
			Status   string `json:"status"`
		} `json:"linux"`
	} `json:"hostsystems"`
	Username      string `json:"username"`
	ManagedRoot   bool   `json:"managed_root"`
	UserID        int64  `json:"user_id"`
	ServiceID     int64  `json:"service_id"`
	LocationID    int    `json:"location_id"`
	MinecraftMode bool   `json:"minecraft_mode"`
	IP            string `json:"ip"`
	Ipv6          string `json:"ipv6"`
	Port          int    `json:"port"`
	QueryPort     int    `json:"query_port"`
	RconPort      int    `json:"rcon_port"`
	Label         string `json:"label"`
	Type          string `json:"type"`
	Memory        string `json:"memory"`
	MemoryMb      int    `json:"memory_mb"`
	Game          string `json:"game"`
	GameHuman     string `json:"game_human"`
	GameSpecific  struct {
		Path          string      `json:"path"`
		UpdateStatus  string      `json:"update_status"`
		LastUpdate    interface{} `json:"last_update"`
		PathAvailable bool        `json:"path_available"`
		Features      struct {
			HasBackups               bool `json:"has_backups"`
			HasWorldBackups          bool `json:"has_world_backups"`
			HasApplicationServer     bool `json:"has_application_server"`
			HasContainerWebsocket    bool `json:"has_container_websocket"`
			HasRcon                  bool `json:"has_rcon"`
			HasFileBrowser           bool `json:"has_file_browser"`
			HasFtp                   bool `json:"has_ftp"`
			HasExpertMode            bool `json:"has_expert_mode"`
			HasPluginSystem          bool `json:"has_plugin_system"`
			HasRestartMessageSupport bool `json:"has_restart_message_support"`
			HasDatabase              bool `json:"has_database"`
		} `json:"features"`
		LogFiles    []interface{} `json:"log_files"`
		ConfigFiles []interface{} `json:"config_files"`
	} `json:"game_specific"`
	Slots       int    `json:"slots"`
	Location    string `json:"location"`
	Credentials struct {
		Ftp struct {
			Hostname string `json:"hostname"`
			Port     int    `json:"port"`
			Username string `json:"username"`
			Password string `json:"password"`
		} `json:"ftp"`
		Mysql struct {
			Hostname string `json:"hostname"`
			Port     int    `json:"port"`
			Username string `json:"username"`
			Password string `json:"password"`
			Database string `json:"database"`
		} `json:"mysql"`
	} `json:"credentials"`
	Settings struct {
		Base struct {
			Dailyrestart string `json:"dailyrestart"`
		} `json:"base"`
		Config struct {
			Pvp          string `json:"pvp"`
			Friendlyfire string `json:"friendlyfire"`
		} `json:"config"`
	} `json:"settings"`
	Quota struct {
		BlockUsage     int `json:"block_usage"`
		BlockSoftlimit int `json:"block_softlimit"`
		BlockHardlimit int `json:"block_hardlimit"`
		FileUsage      int `json:"file_usage"`
		FileSoftlimit  int `json:"file_softlimit"`
		FileHardlimit  int `json:"file_hardlimit"`
	} `json:"quota"`
	Query struct {
		ServerName    string `json:"server_name"`
		ConnectIP     string `json:"connect_ip"`
		Map           string `json:"map"`
		Version       string `json:"version"`
		PlayerCurrent int    `json:"player_current"`
		PlayerMax     int    `json:"player_max"`
		Players       []struct {
			ID     int64  `json:"id"`
			Name   string `json:"name"`
			Bot    bool   `json:"bot"`
			Score  int    `json:"score"`
			Frags  int    `json:"frags"`
			Deaths int    `json:"deaths"`
			Time   int    `json:"time"`
			Ping   int    `json:"ping"`
		} `json:"players"`
	} `json:"query"`
}

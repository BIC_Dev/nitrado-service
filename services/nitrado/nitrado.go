package nitrado

import (
	"fmt"

	"gitlab.com/BIC_Dev/nitrado-service/utils"
)

// BaseURL const
const BaseURL = "https://api.nitrado.net"

// GetAllServices func
func GetAllServices(token string) (response *Response, re *utils.RequestError) {
	url := BaseURL + "/services"

	headers := map[string]string{
		"Authorization": "Bearer " + token,
	}

	response, re = Request("GET", url, nil, nil, headers)

	return
}

// GetService func
func GetService(token string, gameserverID string) (response *Response, re *utils.RequestError) {
	url := BaseURL + "/services/" + gameserverID

	headers := map[string]string{
		"Authorization": "Bearer " + token,
	}

	response, re = Request("GET", url, nil, nil, headers)

	return
}

// GetGameserver func
func GetGameserver(token string, gameserverID string) (response *Response, re *utils.RequestError) {
	url := BaseURL + "/services/" + gameserverID + "/gameservers"

	headers := map[string]string{
		"Authorization": "Bearer " + token,
	}

	response, re = Request("GET", url, nil, nil, headers)

	return
}

// GetGameserverBoosts func
func GetGameserverBoosts(token string, gameserverID string) (*Response, *utils.RequestError) {
	url := BaseURL + "/services/" + gameserverID + "/gameservers/boost/history"

	headers := map[string]string{
		"Authorization": "Bearer " + token,
	}

	morePages := true

	var response *Response
	response = nil
	pageNum := 0

	for morePages {
		params := map[string]string{
			"page": string(pageNum),
		}

		page, re := Request("GET", url, params, nil, headers)

		if re != nil {
			return nil, re
		}

		if response == nil {
			response = page
		} else {
			for _, boost := range page.Data.Boosts {
				response.Data.Boosts = append(response.Data.Boosts, boost)
			}
		}

		if page.Data.CurrentPage == page.Data.PageCount {
			morePages = false
		} else {
			pageNum++
		}
	}

	return response, nil
}

// GetGameserversForAccountName func
func GetGameserversForAccountName(c *utils.Config, accountName string) (*[]string, error) {
	for k, v := range c.Nitrado.Accounts {
		if k == accountName {
			return &v, nil
		}
	}

	return nil, fmt.Errorf("No Nitrado account found by account name")
}

// AccountExists func
func AccountExists(c *utils.Config, accountName string) bool {
	for k := range c.Nitrado.Accounts {
		if k == accountName {
			return true
		}
	}

	return false
}

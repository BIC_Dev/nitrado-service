package models

import (
	"net/http"

	"github.com/jinzhu/gorm"
	"gitlab.com/BIC_Dev/nitrado-service/utils"
	"gitlab.com/BIC_Dev/nitrado-service/utils/db"
)

// ArkRuntime struct
type ArkRuntime struct {
	gorm.Model
	GameserverID int64 `gorm:"index"`
	SuspendDate  int64
	DeleteDate   int64
}

// TableName for db
func (ArkRuntime) TableName() string {
	return "ark_runtime"
}

// Create adds a record to DB
func (t *ArkRuntime) Create(DBStruct db.Interface) *utils.ModelError {
	result := DBStruct.GetDB().Create(&t)

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to create ark runtime entry in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return modelError
	}

	return nil
}

// GetLatest gets the latest record for a GameserverID
func (t *ArkRuntime) GetLatest(DBStruct db.Interface) (*ArkRuntime, *utils.ModelError) {
	var runtime ArkRuntime

	result := DBStruct.GetDB().Model(&ArkRuntime{}).Where("gameserver_id = ?", t.GameserverID).Last(&runtime)

	if result.RecordNotFound() {
		return &runtime, nil
	} else if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to find ark status entry by gameserver_id in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return nil, modelError
	}

	return &runtime, nil
}

// Update the record for a GameserverID
func (t *ArkRuntime) Update(DBStruct db.Interface) *utils.ModelError {
	getResult, getErr := t.GetLatest(DBStruct)

	if getErr != nil {
		return getErr
	}

	if getResult.ID == 0 {
		createErr := t.Create(DBStruct)

		if createErr != nil {
			return createErr
		}

		return nil
	}

	result := DBStruct.GetDB().Model(&t).Where("gameserver_id = ?", t.GameserverID).Updates(map[string]interface{}{
		"suspend_date": t.SuspendDate,
		"delete_date":  t.DeleteDate,
	})

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to update ark runtime entry by gameserver_id in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return modelError
	}

	return nil
}

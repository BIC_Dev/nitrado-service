package models

import (
	"net/http"

	"github.com/jinzhu/gorm"
	"gitlab.com/BIC_Dev/nitrado-service/utils"
	"gitlab.com/BIC_Dev/nitrado-service/utils/db"
)

// ArkStatus struct
type ArkStatus struct {
	gorm.Model
	GameserverID       int64
	AvailabilityStatus string `gorm:"varchar(20)"`
}

// TableName for db
func (ArkStatus) TableName() string {
	return "ark_status"
}

// Create adds a record to DB
func (t *ArkStatus) Create(DBStruct db.Interface) *utils.ModelError {
	result := DBStruct.GetDB().Create(&t)

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to create ark status entry in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return modelError
	}

	return nil
}

// Get gets the record for a GameserverID
func (t *ArkStatus) Get(DBStruct db.Interface) (*ArkStatus, *utils.ModelError) {
	var status *ArkStatus

	result := DBStruct.GetDB().Model(&ArkStatus{}).Where("gameserver_id = ?", t.GameserverID).First(&status)

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to find ark status entry by gameserver_id in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return nil, modelError
	}

	return status, nil
}

// Update the record for a GameserverID
func (t *ArkStatus) Update(DBStruct db.Interface) *utils.ModelError {
	result := DBStruct.GetDB().Model(&ArkStatus{}).Where("gameserver_id = ?", t.GameserverID).Update(&t)

	if gorm.IsRecordNotFoundError(result.Error) {
		createErr := t.Create(DBStruct)

		if createErr != nil {
			return createErr
		}
	} else if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to update ark status entry by gameserver_id in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return modelError
	}

	return nil
}

package models

import (
	"fmt"
	"net/http"

	"github.com/jinzhu/gorm"
	"gitlab.com/BIC_Dev/nitrado-service/utils"
	"gitlab.com/BIC_Dev/nitrado-service/utils/db"
)

// ArkPlayers struct
type ArkPlayers struct {
	gorm.Model
	GameserverID   int64 `gorm:"index"`
	CurrentPlayers int
	MaxPlayers     int
}

// TableName for the db
func (ArkPlayers) TableName() string {
	return "ark_players"
}

// Create adds a record to DB
func (t *ArkPlayers) Create(DBStruct db.Interface) *utils.ModelError {
	result := DBStruct.GetDB().Create(&t)

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to create ark players entry in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return modelError
	}

	return nil
}

// GetLatest gets the latest record for a GameserverID
func (t *ArkPlayers) GetLatest(DBStruct db.Interface) (*ArkPlayers, *utils.ModelError) {
	var players ArkPlayers

	result := DBStruct.GetDB().Model(&ArkPlayers{}).Where("gameserver_id = ?", t.GameserverID).Last(&players)

	if gorm.IsRecordNotFoundError(result.Error) {
		return &players, nil
	}

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage(fmt.Sprintf("Unable to find ark players entry by gameserver_id in DB: %d", t.GameserverID))
		modelError.SetStatus(http.StatusBadRequest)
		return nil, modelError
	}

	return &players, nil
}

// Update or create the record for a GameserverID
func (t *ArkPlayers) Update(DBStruct db.Interface) *utils.ModelError {
	getResult, getErr := t.GetLatest(DBStruct)

	if getErr != nil {
		return getErr
	}

	if getResult.ID == 0 {
		createErr := t.Create(DBStruct)

		if createErr != nil {
			return createErr
		}

		return nil
	}

	result := DBStruct.GetDB().Model(&t).Where("gameserver_id = ?", t.GameserverID).Updates(map[string]interface{}{
		"current_players": t.CurrentPlayers,
		"max_players":     t.MaxPlayers,
	})

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to update ark players entry by gameserver_id in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return modelError
	}

	return nil
}

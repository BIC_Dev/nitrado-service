package models

import (
	"net/http"

	"github.com/jinzhu/gorm"
	"gitlab.com/BIC_Dev/nitrado-service/utils"
	"gitlab.com/BIC_Dev/nitrado-service/utils/db"
)

// ArkBoosts struct
type ArkBoosts struct {
	gorm.Model
	GameserverID int64  `gorm:"index"`
	BoosterName  string `gorm:"type:varchar(100)"`
	BoostMessage string `gorm:"type:text"`
	BoostTime    int
	BoostPrice   float64
}

// TableName for db
func (ArkBoosts) TableName() string {
	return "ark_boosts"
}

// Create adds a record to DB
func (t *ArkBoosts) Create(DBStruct db.Interface) *utils.ModelError {
	result := DBStruct.GetDB().Create(&t)

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to create ark boost entry in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return modelError
	}

	return nil
}

// GetAllByGameserverID gets all boosts for a GameserverID
func (t *ArkStatus) GetAllByGameserverID(DBStruct db.Interface) ([]*ArkBoosts, *utils.ModelError) {
	var boosts []*ArkBoosts

	result := DBStruct.GetDB().Model(&ArkBoosts{}).Where("gameserver_id = ?", t.GameserverID).Find(&boosts)

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to find ark boost entries by gameserver_id in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return nil, modelError
	}

	return boosts, nil
}

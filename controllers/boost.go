package controllers

import "net/http"

// GetAllBoosts route
// GET /boost/{gameserver_id}
// Headers:
//		Service-Token
//		Account-Name
func (c *Controller) GetAllBoosts(w http.ResponseWriter, r *http.Request) {

}

// GetNewBoosts route
// GET /boost/{gameserver_id}/new
// Headers:
//		Service-Token
//		Account-Name
func (c *Controller) GetNewBoosts(w http.ResponseWriter, r *http.Request) {

}

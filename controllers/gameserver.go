package controllers

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/BIC_Dev/nitrado-service/models"
	"gitlab.com/BIC_Dev/nitrado-service/services/nitrado"
	"gitlab.com/BIC_Dev/nitrado-service/utils"
	"gitlab.com/BIC_Dev/nitrado-service/utils/db"
	"gitlab.com/BIC_Dev/nitrado-service/viewmodels"
)

// GetGameserver route
// GET /gameserver/{gameserver_id}
// Headers:
//		Service-Token
//		Account-Name
func (c *Controller) GetGameserver(w http.ResponseWriter, r *http.Request) {
	c.Log.Log("PROCESS: GET Gameserver route called", c.Log.LogInformation)

	if r.Header.Get("Service-Token") != c.ServiceToken {
		c.Log.Log("BLOCKED: Invalid service token for GET Gameserver request", c.Log.LogMedium)
		errorResponse := viewmodels.NewErrorResponse(http.StatusUnauthorized, "Invalid service token", "Invalid service token for GET Gameserver request")
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	accountName := r.Header.Get("Account-Name")

	token, ok := c.NitradoAuthTokens[accountName]

	if !ok {
		c.Log.Log("ERROR: Nitrado auth token does not exist for GET Gameserver request", c.Log.LogMedium)
		errorResponse := viewmodels.NewErrorResponse(http.StatusUnauthorized, "Missing nitrado auth token for account name", "Nitrado auth token does not exist for GET Gameserver request")
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	vars := mux.Vars(r)
	gameserverID := vars["gameserver_id"]

	gameserver, gameserverErr := nitrado.GetGameserver(token, gameserverID)

	if gameserverErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: %s: %s", gameserverErr.GetMessage(), gameserverErr.Error()), c.Log.LogMedium)
		errorResponse := viewmodels.NewErrorResponse(http.StatusBadRequest, gameserverErr.Error(), gameserverErr.GetMessage())
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	dbInterface, dbErr := db.GetDB(c.Config)

	if dbErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: %s: %s", dbErr.GetMessage(), dbErr.Error()), c.Log.LogHigh)
		errorResponse := viewmodels.NewErrorResponse(http.StatusBadRequest, dbErr.Error(), dbErr.GetMessage())
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	defer dbInterface.GetDB().Close()

	players, playersErr := handlePlayers(gameserver, c, dbInterface)

	if playersErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: %s: %s", playersErr.GetMessage(), playersErr.Error()), c.Log.LogMedium)
		errorResponse := viewmodels.NewErrorResponse(http.StatusBadRequest, playersErr.Error(), playersErr.GetMessage())
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	runtimeStruct := models.ArkRuntime{
		GameserverID: gameserver.Data.Gameserver.ServiceID,
	}

	c.Log.Log(fmt.Sprintf("PROCESS: Getting runtime data for Gameserver: %d", gameserver.Data.Gameserver.ServiceID), c.Log.LogInformation)
	runtime, runtimeErr := runtimeStruct.GetLatest(dbInterface)

	var response viewmodels.GameserverResponse

	response.Gameserver = viewmodels.Gameserver{
		Status:           gameserver.Data.Gameserver.Status,
		LastStatusChange: gameserver.Data.Gameserver.LastStatusChange,
		MustBeStarted:    gameserver.Data.Gameserver.MustBeStarted,
		WebsocketToken:   gameserver.Data.Gameserver.WebsocketToken,
		Username:         gameserver.Data.Gameserver.Username,
		UserID:           gameserver.Data.Gameserver.UserID,
		ServiceID:        gameserver.Data.Gameserver.ServiceID,
		LocationID:       gameserver.Data.Gameserver.LocationID,
		MinecraftMode:    gameserver.Data.Gameserver.MinecraftMode,
		IP:               gameserver.Data.Gameserver.IP,
		Ipv6:             gameserver.Data.Gameserver.Ipv6,
		Port:             gameserver.Data.Gameserver.Port,
		QueryPort:        gameserver.Data.Gameserver.QueryPort,
		RconPort:         gameserver.Data.Gameserver.RconPort,
		Label:            gameserver.Data.Gameserver.Label,
		Type:             gameserver.Data.Gameserver.Type,
		Memory:           gameserver.Data.Gameserver.Memory,
		MemoryMb:         gameserver.Data.Gameserver.MemoryMb,
		Game:             gameserver.Data.Gameserver.Game,
		GameHuman:        gameserver.Data.Gameserver.GameHuman,
		Slots:            gameserver.Data.Gameserver.Slots,
		Query:            viewmodels.Query(gameserver.Data.Gameserver.Query),
	}

	response.Gameserver.Query.PlayerCurrent = players.CurrentPlayers
	response.Gameserver.Query.PlayerMax = players.MaxPlayers

	if runtimeErr == nil {
		response.Gameserver.SuspendDate = runtime.SuspendDate
		response.Gameserver.DeleteDate = runtime.DeleteDate
	}

	SendJSONResponse(w, response, http.StatusOK)
	return
}

func handlePlayers(gameserver *nitrado.Response, c *Controller, dbInterface db.Interface) (*models.ArkPlayers, *utils.ModelError) {
	if gameserver.Data.Gameserver.Query.PlayerMax != 0 {
		players := models.ArkPlayers{
			GameserverID:   gameserver.Data.Gameserver.ServiceID,
			CurrentPlayers: gameserver.Data.Gameserver.Query.PlayerCurrent,
			MaxPlayers:     gameserver.Data.Gameserver.Query.PlayerMax,
		}

		c.Log.Log(fmt.Sprintf("PROCESS: Saving player data for Gameserver: %d", gameserver.Data.Gameserver.ServiceID), c.Log.LogInformation)

		playersErr := players.Update(dbInterface)

		if playersErr != nil {
			return nil, playersErr
		}

		return &players, nil
	}

	players := models.ArkPlayers{
		GameserverID: gameserver.Data.Gameserver.ServiceID,
	}

	c.Log.Log(fmt.Sprintf("PROCESS: Retreiving previous player data for Gameserver: %d", gameserver.Data.Gameserver.ServiceID), c.Log.LogInformation)

	latestPlayers, playersErr := players.GetLatest(dbInterface)

	if playersErr != nil {
		return nil, playersErr
	}

	return latestPlayers, nil
}

package controllers

import (
	"fmt"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/BIC_Dev/nitrado-service/models"
	"gitlab.com/BIC_Dev/nitrado-service/services/nitrado"
	"gitlab.com/BIC_Dev/nitrado-service/utils/db"
	"gitlab.com/BIC_Dev/nitrado-service/viewmodels"
)

// GetOneService route
// GET /service/{gameserver_id}
// Headers:
//		Service-Token
//		Account-Name
func (c *Controller) GetOneService(w http.ResponseWriter, r *http.Request) {
	c.Log.Log("PROCESS: GET service route called", c.Log.LogInformation)

	if r.Header.Get("Service-Token") != c.ServiceToken {
		c.Log.Log("BLOCKED: Invalid service token for GET service request", c.Log.LogMedium)
		errorResponse := viewmodels.NewErrorResponse(http.StatusUnauthorized, "Invalid service token", "Invalid service token for GET service request")
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	accountName := r.Header.Get("Account-Name")

	token, ok := c.NitradoAuthTokens[accountName]

	if !ok {
		c.Log.Log("ERROR: Nitrado auth token does not exist for GET service request", c.Log.LogMedium)
		errorResponse := viewmodels.NewErrorResponse(http.StatusUnauthorized, "Missing nitrado auth token for account name", "Nitrado auth token does not exist for GET service request")
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	vars := mux.Vars(r)
	gameserverID := vars["gameserver_id"]

	service, requestErr := nitrado.GetService(token, gameserverID)

	if requestErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Failed to GET service with error: %s", requestErr.Error()), c.Log.LogMedium)
		errorResponse := viewmodels.NewErrorResponse(http.StatusUnauthorized, requestErr.Error(), requestErr.GetMessage())
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	dbInterface, dbErr := db.GetDB(c.Config)

	if dbErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: %s: %s", dbErr.GetMessage(), dbErr.Error()), c.Log.LogHigh)
		errorResponse := viewmodels.NewErrorResponse(http.StatusBadRequest, dbErr.Error(), dbErr.GetMessage())
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	defer dbInterface.GetDB().Close()

	currentTime := time.Now().Unix()

	runtime := models.ArkRuntime{
		GameserverID: service.Data.Service.ID,
		SuspendDate:  currentTime + int64(service.Data.Service.SuspendingIn),
		DeleteDate:   currentTime + int64(service.Data.Service.DeletingIn),
	}

	runtimeErr := runtime.Update(dbInterface)

	if runtimeErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: %s: %s", runtimeErr.GetMessage(), runtimeErr.Error()), c.Log.LogMedium)
		errorResponse := viewmodels.NewErrorResponse(http.StatusInternalServerError, runtimeErr.Error(), runtimeErr.GetMessage())
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	aService := viewmodels.Service(service.Data.Service)

	response := viewmodels.ServiceResponse{
		Service: aService,
	}

	SendJSONResponse(w, response, http.StatusOK)
	return
}

// GetAllServices route
// GET /service
// Headers:
//		Service-Token
//		Account-Name
func (c *Controller) GetAllServices(w http.ResponseWriter, r *http.Request) {
	c.Log.Log("PROCESS: GET all services route called", c.Log.LogInformation)

	if r.Header.Get("Service-Token") != c.ServiceToken {
		c.Log.Log("BLOCKED: Invalid service token for GET all services request", c.Log.LogMedium)
		errorResponse := viewmodels.NewErrorResponse(http.StatusUnauthorized, "Invalid service token", "Invalid service token for GET all services request")
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	accountName := r.Header.Get("Account-Name")

	token, ok := c.NitradoAuthTokens[accountName]

	if !ok {
		c.Log.Log("ERROR: Nitrado auth token does not exist for GET all services request", c.Log.LogMedium)
		errorResponse := viewmodels.NewErrorResponse(http.StatusUnauthorized, "Missing nitrado auth token for account name", "Nitrado auth token does not exist for GET all services request")
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	services, requestErr := nitrado.GetAllServices(token)

	if requestErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Failed to GET all services with error: %s", requestErr.Error()), c.Log.LogMedium)
		errorResponse := viewmodels.NewErrorResponse(http.StatusUnauthorized, requestErr.Error(), requestErr.GetMessage())
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	dbInterface, dbErr := db.GetDB(c.Config)

	if dbErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: %s: %s", dbErr.GetMessage(), dbErr.Error()), c.Log.LogHigh)
		errorResponse := viewmodels.NewErrorResponse(http.StatusBadRequest, dbErr.Error(), dbErr.GetMessage())
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	defer dbInterface.GetDB().Close()

	var servicesResponse viewmodels.ServicesResponse

	for _, service := range services.Data.Services {
		var runtime models.ArkRuntime

		currentTime := time.Now().Unix()

		runtime.GameserverID = service.ID
		runtime.SuspendDate = currentTime + int64(service.SuspendingIn)
		runtime.DeleteDate = currentTime + int64(service.DeletingIn)

		c.Log.Log(fmt.Sprintf("PROCESS: Updating runtime data for Gameserver ID: %d", service.ID), c.Log.LogInformation)

		runtime.Update(dbInterface)

		servicesResponse.Services = append(servicesResponse.Services, viewmodels.Service(service))
	}

	SendJSONResponse(w, servicesResponse, http.StatusOK)
	return
}

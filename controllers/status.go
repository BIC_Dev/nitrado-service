package controllers

import (
	"net/http"

	"gitlab.com/BIC_Dev/nitrado-service/viewmodels"
)

// GetStatus responds with the availability status of this service
func (c *Controller) GetStatus(w http.ResponseWriter, r *http.Request) {
	status := viewmodels.GetStatusResponse{
		Status:  http.StatusOK,
		Message: "Nitrado service is available",
	}

	SendJSONResponse(w, status, status.Status)
}

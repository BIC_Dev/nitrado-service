module gitlab.com/BIC_Dev/nitrado-service

go 1.14

require (
	github.com/gorilla/mux v1.7.4
	github.com/jinzhu/gorm v1.9.14
	gopkg.in/yaml.v2 v2.3.0
)

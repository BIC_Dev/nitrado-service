package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strconv"

	"gitlab.com/BIC_Dev/nitrado-service/controllers"
	"gitlab.com/BIC_Dev/nitrado-service/models"
	"gitlab.com/BIC_Dev/nitrado-service/routes"
	"gitlab.com/BIC_Dev/nitrado-service/utils"
	"gitlab.com/BIC_Dev/nitrado-service/utils/db"
)

// Service information for the service
type Service struct {
	Config *utils.Config
	Log    *utils.Log
}

func main() {
	env := os.Getenv("ENVIRONMENT")
	logLevel, err := strconv.Atoi(os.Getenv("LOG_LEVEL"))

	if err != nil {
		log.Fatal(err.Error())
	}

	service := Service{
		Config: utils.GetConfig(env),
		Log:    utils.InitLog(logLevel),
	}

	accountsJSON, accountsJSONExists := os.LookupEnv("NITRADO_TOKENS")

	if !accountsJSONExists {
		service.Log.Log("ERROR: Missing Nitrado account credentials", service.Log.LogFatal)
	}

	serviceToken, serviceTokenExists := os.LookupEnv("SERVICE_TOKEN")

	if !serviceTokenExists {
		service.Log.Log("ERROR: Missing service token", service.Log.LogFatal)
	}

	if os.Getenv("MIGRATE") == "TRUE" {
		migrationErrors := service.migrateTables(models.ArkBoosts{}, models.ArkPlayers{}, models.ArkRuntime{}, models.ArkStatus{})

		if migrationErrors != nil {
			service.Log.Log(migrationErrors, service.Log.LogFatal)
		}
	}

	var nitradoAccounts map[string]string

	unmarshalErr := json.Unmarshal([]byte(accountsJSON), &nitradoAccounts)

	if unmarshalErr != nil {
		service.Log.Log(fmt.Sprintf("ERROR: Could not unmarshal nitrado accounts with error: %s", unmarshalErr.Error()), service.Log.LogFatal)
	}

	controller := controllers.Controller{
		Config:            service.Config,
		Log:               service.Log,
		NitradoAuthTokens: nitradoAccounts,
		ServiceToken:      serviceToken,
	}

	service.Log.Log("PROCESS: Setting up router", service.Log.LogInformation)

	router := routes.GetRouter()
	routes.AddRoutes(router, &controller)

	service.Log.Log("SUCCESS: Set up router", service.Log.LogInformation)

	service.Log.Log("PROCESS: Starting MUX listener", service.Log.LogInformation)
	routes.StartListener(router)
}

func (s *Service) migrateTables(tables ...interface{}) []*utils.ModelError {
	s.Log.Log("Migrating tables: twitter, facebook", s.Log.LogInformation)
	var migrationErrors []*utils.ModelError

	dbStruct, dbErr := db.GetDB(s.Config)

	if dbErr != nil {
		s.Log.Log(fmt.Sprintf("%s: %s", dbErr.GetMessage(), dbErr.Error()), s.Log.LogFatal)
	}

	defer dbStruct.GetDB().Close()

	for _, table := range tables {
		migrationError := dbStruct.Migrate(table)

		if migrationError != nil {
			migrationErrors = append(migrationErrors, migrationError)
		}
	}

	if len(migrationErrors) > 0 {
		return migrationErrors
	}

	return nil
}

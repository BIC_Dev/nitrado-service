package viewmodels

/*
// GameserverResponse struct
type GameserverResponse struct {
	Gameserver struct {
		ID             int    `json:"id"`
		Status         string `json:"status"`
		PlayersCurrent int    `json:"players_current"`
		PlayersMax     int    `json:"players_max"`
		SuspendDate    int64  `json:"suspend_date"`
		DeleteDate     int64  `json:"delete_date"`
	}
}
*/

// GameserverResponse struct
type GameserverResponse struct {
	Gameserver Gameserver `json:"gameserver"`
}

// Gameserver struct
type Gameserver struct {
	Status           string      `json:"status"`
	LastStatusChange int         `json:"last_status_change"`
	MustBeStarted    bool        `json:"must_be_started"`
	WebsocketToken   string      `json:"websocket_token"`
	Username         string      `json:"username"`
	UserID           int64       `json:"user_id"`
	ServiceID        int64       `json:"service_id"`
	LocationID       int         `json:"location_id"`
	MinecraftMode    bool        `json:"minecraft_mode"`
	IP               string      `json:"ip"`
	Ipv6             interface{} `json:"ipv6"`
	Port             int         `json:"port"`
	QueryPort        int         `json:"query_port"`
	RconPort         int         `json:"rcon_port"`
	Label            string      `json:"label"`
	Type             string      `json:"type"`
	Memory           string      `json:"memory"`
	MemoryMb         int         `json:"memory_mb"`
	Game             string      `json:"game"`
	GameHuman        string      `json:"game_human"`
	Slots            int         `json:"slots"`
	Query            Query       `json:"query"`
	SuspendDate      int64       `json:"suspend_date"`
	DeleteDate       int64       `json:"delete_date"`
}

// Query struct
type Query struct {
	ServerName    string `json:"server_name"`
	ConnectIP     string `json:"connect_ip"`
	Map           string `json:"map"`
	Version       string `json:"version"`
	PlayerCurrent int    `json:"player_current"`
	PlayerMax     int    `json:"player_max"`
	Players       []struct {
		ID     int64  `json:"id"`
		Name   string `json:"name"`
		Bot    bool   `json:"bot"`
		Score  int    `json:"score"`
		Frags  int    `json:"frags"`
		Deaths int    `json:"deaths"`
		Time   int    `json:"time"`
		Ping   int    `json:"ping"`
	} `json:"players"`
}

package viewmodels

// ServicesResponse struct
type ServicesResponse struct {
	Services []Service `json:"services"`
}

// ServiceResponse struct
type ServiceResponse struct {
	Service Service `json:"service"`
}

// Service struct
type Service struct {
	ID                    int64  `json:"id"`
	LocationID            int    `json:"location_id"`
	Status                string `json:"status"`
	WebsocketToken        string `json:"websocket_token"`
	UserID                int64  `json:"user_id"`
	Comment               string `json:"comment"`
	AutoExtension         bool   `json:"auto_extension"`
	AutoExtensionDuration int    `json:"auto_extension_duration"`
	Type                  string `json:"type"`
	TypeHuman             string `json:"type_human"`
	Details               struct {
		Address       string `json:"address"`
		Name          string `json:"name"`
		Game          string `json:"game"`
		PortlistShort string `json:"portlist_short"`
		FolderShort   string `json:"folder_short"`
		Slots         int    `json:"slots"`
	} `json:"details"`
	StartDate    string   `json:"start_date"`
	SuspendDate  string   `json:"suspend_date"`
	DeleteDate   string   `json:"delete_date"`
	SuspendingIn int      `json:"suspending_in"`
	DeletingIn   int      `json:"deleting_in"`
	Username     string   `json:"username"`
	Roles        []string `json:"roles"`
}
